
public class OverSpcRemover{
    public static void main(String[] args) {

            String frs=args[0];                                                 //Lettura frase
            frs=overSpcRemover(frs);
            System.out.println(frs);
    }

    static String overSpcRemover(String frs){

            char[] frsChar=frs.toCharArray();
            int size=frsChar.length, nSpc=0;
            char spc=(char)32;
                
            for(int i=0;i<size;i++){ 
                    if(frsChar[i]==spc)                                         //Individuazione e conservazione del primo spazio
                        i++;                        
                    for(int k=i;frsChar[k]==spc;k++)                            //Conteggio degli spazi non necessari
                            nSpc++;

                    size-=nSpc;
                    for(int k=i;k<size;k++){                                    //Trasporto dei caratteri al posto degli spazi
                            try {
                                frsChar[k]=frsChar[k+nSpc];
                            
                            } catch (ArrayIndexOutOfBoundsException e) {        
                                k++;
                            }                                                   
                    }
                    //System.out.println(nSpc);  
                    nSpc=0;                 
            }
            char[] frsCharOut=new char[size];
            for(int i=0;i<size;i++)
                    frsCharOut[i]=frsChar[i];

            String frsOut= new String(frsCharOut);                                 //Parsing dell'array di char a stringa
            return frsOut;    
    }                 
}